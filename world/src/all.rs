#[derive(Copy, Clone)]
pub enum ForestKind {
    Palm,
    Oak,
    Pine,
    SnowPine,
}
